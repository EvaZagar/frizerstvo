<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/skripta.js"></script>
    <link rel="stylesheet" type="text/css" href="css/stili.css">
    <title>Sign up</title>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-light navbar-light">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link pisava" href="home.php" style="font-size: 30px;">HairStyling</a>
        </li>
    </ul>
</nav>
<div class="container" style="padding: 35px">
    <div class="row">
        <div class="col-lg-12">
            <div id="opozorilo"></div>
        </div>
    </div>
</div>
<div class="container" style="width:500px;">
    <h3 class="text-center">Sign up</h3><br>
    <form method="post" class="text-center" onsubmit="return validacijaRegistracija()">
        <label for="username">Name</label>
        <input type="text" name="username" id="username" class="form-control"/>
        <br>
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control"/>
        <br>
        <label for="phone_number">Phone number</label>
        <input type="number" name="phone_number" id="phone_number" class="form-control"/>
        <br>
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control"/>
        <br>
        <label for="password2">Repeat your password</label>
        <input type="password" name="password2" id="password2" class="form-control"/>
        <br>
        <input type="submit" name="register" class="btn btn-info" value="Register"/>
    </form>
    <p class="text-center">Already have an account?</p>
    <p class="text-center">Login <a href="login.php">here</a>!</p>
</div>
<br>
<?php

require 'config.php';

if(isset($_POST['register'])){

    $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
    $phone_number = !empty($_POST['phone_number']) ? trim($_POST['phone_number']) : null;
    $pass = !empty($_POST['password']) ? trim($_POST['password']) : null;
    $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';

    if (preg_match($pattern, $email) === 1) {
        $sql = "SELECT COUNT(username) AS num FROM users WHERE username = :username";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':username', $username);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($row['num'] > 0){
            echo "<script>$('#opozorilo').addClass('alert alert-danger');
                $('#opozorilo').text('This email already exists.');</script>";
        }
        $passwordHash = password_hash($pass, PASSWORD_BCRYPT, array("cost" => 12));

        $sql = "INSERT INTO users (username, email, password, phone_number) VALUES (:username, :email, :password, :phone_number)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':phone_number', $phone_number);
        $stmt->bindValue(':password', $passwordHash);
        $result = $stmt->execute();

        if($result){
            header('Location: servicePick.php');
        }

    }else{
        echo "<script>$('#opozorilo').addClass('alert alert-danger');
                $('#opozorilo').text('Invalid email');</script>";
    }

}

?>

</body>
