<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/skripta.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/stili.css">
    <title>LogIn</title>
</head>
<body>
<?php

require 'config.php';

if(isset($_POST['login'])){
//Retrieve the field values from our login form.
    $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
    $passwordAttempt = !empty($_POST['password']) ? trim($_POST['password']) : null;

//Retrieve the user account information for the given username.
    $sql = "SELECT id, username, email, phone_number, password FROM users WHERE email = :email";
    $stmt = $pdo->prepare($sql);

//Bind value.
    $stmt->bindValue(':email', $email);

//Execute.
    $stmt->execute();

//Fetch row.
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

//If $row is FALSE.
    if($user === false){
//Could not find a user with that username!
//PS: You might want to handle this error in a more user-friendly manner!
        echo '<div class="container" style="padding: 35px">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">Uporabnik ne obstaja!</div>
                        </div>
                    </div>
                </div>';
} else{
//User account found. Check to see if the given password matches the
//password hash that we stored in our users table.

//Compare the passwords.
        $validPassword = password_verify($passwordAttempt, $user['password']);

//If $validPassword is TRUE, the login has been successful.
        if($validPassword){

            session_start();
//Provide the user with a login session.
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['username'] = $user['username'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['phone_number'] = $user['phone_number'];

//Redirect to our protected page, which we called home.php
            header('Location: servicePick.php');
            exit;

        } else{
//$validPassword was FALSE. Passwords do not match.
            echo '<div class="container" style="padding: 35px">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">Napačno geslo!</div>
                        </div>
                    </div>
                </div>';
        }
    }

}

?>
<div class="container text-center" style="width:500px; margin-top:60px">
    <div class="row">
        <div class="col-lg-12">
            <h3>Login</h3><br>
            <div class="row">
                <div class="col-lg-12">
                    <form method="post" onsubmit="return validacijaPrijava()">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control">
                        <br>
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control">
                        <br>
                        <input type="submit" name="login" class="btn btn-info" value="Login">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <p class="text-center">Don't have an account yet?</p>
            <p class="text-center">Register <a href="signUp.php">here</a>!</p>
        </div>
        <div class="col-lg-12">
            <form action="employeeLogin.php">
                <input type="submit" class="btn btn-dark text-center" style="margin-top:20px;" value="Employee login"/>
            </form>
        </div>
    </div>
</div>

</body>