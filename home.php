<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/skripta.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/stili.css">
    <title>Title</title>
</head>
<body>
<body class="bg-dark">
<div class="jumbotron" style = "background-image: linear-gradient(to bottom, rgba(255,255,255,0.6) 0%,rgba(255,255,255,0.9) 100%), url(http://studiokdesignteam.com/wp-content/uploads/2016/09/bottom_items.png)">
    <h1 class="display-4">HairStyling</h1>
    <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
    <hr class="my-4">
    <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
    <p class="lead">
        <a class="btn btn-dark btn-lg" href="login.php" role="button">Make an appointment</a>
    </p>
</div>
<div class="container">
    <div class="row">
        <?php
        $access_token="27184376939.1677ed0.8da8fd3ff49e4fee8e4a53da048f13af";
        $photo_count=4;

        $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
        $json_link.="access_token={$access_token}&count={$photo_count}";
        $json = file_get_contents($json_link);
        $obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);

        $i = 0;
        foreach ($obj['data'] as $post) {

            $pic_text=$post['caption']['text'];
            $pic_link=$post['link'];
            $pic_like_count=$post['likes']['count'];
            $pic_comment_count=$post['comments']['count'];
            $pic_src=str_replace("http://", "https://", $post['images']['standard_resolution']['url']);
            if($i < 3) {
                echo '<div class="col-lg-4">';
                    echo '<div class="card" style="width: 18rem; height: 18rem; margin-top: 80px; margin-right: 3px">';
                        echo "<a href='{$pic_link}'>"."<img class='card-img-top' src='{$pic_src}'>"."</a>";
                        echo '<div class="card-body">';
                            echo '<p class="card-text">'.$pic_text.'</p>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
                $i++;
            }else{
                echo '</row>';
                echo '<div class="row">';
                $i = 0;
                continue;
            }
        }
            ?>
    </div>
    </div>
</body>