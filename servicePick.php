<?php
session_start();
?>
<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/stili.css">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <title>Prijava</title>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-light navbar-light">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="navbar-brand" href="servicePick.php">Make an appointment</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="btn btn-dark" href="profile.php" role="button" style="margin-right: 5px">My profile</a>
            </li>
            <li class="nav-item">
            <li class="nav-item">
                <a href="logout.php" class="btn btn-info">LogOut</a>
            </li>
            </li>
        </ul>
    </nav>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <form method="post" action="">
                <?php
                require 'config.php';
                $sql="SELECT id, name FROM servicecategory";

                foreach ($pdo->query($sql) as $row){
                    $sql3 = "SELECT COUNT(id) AS num FROM service WHERE fk_servicecategory = $row[id]";
                    $stmt = $pdo->prepare($sql3);
                    $stmt->execute();
                    $r = $stmt->fetch(PDO::FETCH_ASSOC);

                    if($r['num'] == 0){
                        continue;
                    }

                    echo '<div class = "panel panel-default" style="margin-top: 50px">';
                    echo '<div class = "panel-heading">';
                    echo '<p class = "panel-title" style="font-size: 30px">';
                    echo "<i class='fa fa-angle-down'><a data-toggle='collapse' href = '#$row[name]' style='color:black; text-decoration: none;margin-left: 20px'>$row[name]</a></i>";
                    echo'</div>';
                    echo"<div id = '$row[name]' class='panel-collapse collapse'>";
                    echo '<ul class = "list-group">';

                    $sql2 = "SELECT id, name, price, duration FROM service WHERE fk_servicecategory = $row[id] ";
                    foreach ($pdo->query($sql2) as $row) {
                        echo"<li class='list-group-item list-group-item-info' style='margin-left: 20px'>$row[name]<input type='checkbox' name='izbire[]' value='$row[id]' style='float:right; margin-right: 40px'><i class='fa fa-eur' style='float:right; margin-right: 110px'>$row[price]</i><i class='fa fa-clock-o' style='float:right; margin-right: 110px'>$row[duration]</i></li>";
                    }
                    echo'</ul>';
                    echo '</div>';
                    echo '</div>';

                }
                ?>
                <div class="row">
                    <div class="col-lg-6">
                        <label for="disclaimer" style="margin-top: 80px">Disclaimer</label>
                        <textarea name="disclaimer" id="disclaimer" class="form-control" rows="6"></textarea>
                        <br>
                        <label for="file" style="margin-top: 30px">Upload a picture</label>
                        <input type="file" class="form-control-file" name="file" id="file"/>
                        <br>
                        <input type="submit" class="btn btn-info text-center" name='save' value="Choose a date" style="margin-top:20px; margin-bottom: 20px"/>
                    </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
$vsota = 0.0;
$trajanje = 0;
$array_izbire = [];
if(isset($_POST['save'])){

    if(!empty($_POST['izbire'])) {

        foreach($_POST['izbire'] as $value){
            $sql = "SELECT price FROM service WHERE id = $value";
            $stmt = $pdo->query($sql);
            $result = $stmt->fetch();
            $vsota = $vsota + $result[0];
            $array_izbire[] = $value;
        }
        foreach($_POST['izbire'] as $value){
            $sql = "SELECT duration FROM service WHERE id = $value";
            $stmt = $pdo->query($sql);
            $result = $stmt->fetch();
            $trajanje = $trajanje + $result[0];
        }
        echo 'vsota: '.$vsota;
        echo 'trajanje: '.$trajanje;
        $_SESSION['duration'] = $trajanje;
        $_SESSION['array_izbire'] = $array_izbire;
        header('Location:cal.php');
    }
}
?>
</body>

