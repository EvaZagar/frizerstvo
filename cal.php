<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <title>Calendar</title>
    <script>
        $(document).ready(function() {
            var calendar = $('#calendar').fullCalendar({
                editable:false,
                header:{
                    left:'prev,next',
                    center:'title',
                    right:'',
                },
                defaultView:'agendaDay',
                events:'load.php',
                selectable:true,
                select: function(start, end) {
                    var prob = prompt("Potrdi");
                    var title = "<?php echo $_SESSION['username']?>";
                    if(prob) {
                        function pad2(number, length) {
                            var str = '' + number;
                            while (str.length < length) {
                                str = '0' + str;
                            }
                            return str;
                        }
                        end = new Date(start-1);
                        end.setMinutes(end.getMinutes() + <?php echo $_SESSION['duration']?>);
                        var end_date = end.getFullYear()
                            + '-' + pad2(end.getMonth()+1, 2)
                            + '-' + pad2(end.getDate(), 2)
                            + ' ' + pad2(end.getHours(), 2)
                            + ':' + pad2(end.getMinutes(), 2);
                        calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: start,
                                end: end,
                                allDay: false
                            },
                            true
                        );
                        var start_d = new Date(start);
                        var start_date = start_d.getFullYear()
                            + '-' + pad2(start_d.getMonth()+1, 2)
                            + '-' + pad2(start_d.getDate(), 2)
                            + ' ' + pad2(start_d.getHours(), 2)
                            + ':' + pad2(start_d.getMinutes(), 2);
                        var dat = new Date(start);
                        var dat = dat.getFullYear()
                            + '-' + pad2(dat.getMonth()+1)
                            + '-' + pad2(dat.getDate());
                        $.ajax({
                            url:"insert.php",
                            type:"POST",
                            data:{title:title, start:start_date, end:end_date, date:dat},
                            success:function(){
                                $('#alert').css('visibility', 'visible');
                                $('#alert').text('Your appointment was made');
                            }
                        })
                    }
                },

            });
        });

    </script>
</head>
<body>
<div class="container" style="margin-top: 30px">
    <div id="alert" class="alert alert-success" style="visibility: hidden"></div>
</div>
<div class="container" style="margin-top: 30px">
    <div id="calendar"></div>
</div>
</body>
</html>
