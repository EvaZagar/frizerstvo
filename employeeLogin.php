<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/skripta.js"></script>
    <link rel="stylesheet" type="text/css" href="css/stili.css">
    <title>Prijava</title>
</head>
<body>
<?php
session_start();

require 'config.php';

if(isset($_POST['login'])){
//Retrieve the field values from our login form.
    $password = !empty($_POST['password']) ? trim($_POST['password']) : null;

//Retrieve the user account information for the given username.
    $sql = "SELECT id, name, lastname FROM employees WHERE password = :password";
    $stmt = $pdo->prepare($sql);

//Bind value.
    $stmt->bindValue(':password', $password);

//Execute.
    $stmt->execute();

//Fetch row.
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

//If $row is FALSE.
    if($user === false){
//Could not find a user with that username!
//PS: You might want to handle this error in a more user-friendly manner!
        echo '<div class="container" style="padding: 35px">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">Wrong password!</div>
                        </div>
                    </div>
                </div>';
}else{

        $_SESSION['employee_id'] = $user['id'];
        $_SESSION['name'] = $user['name'];
        $_SESSION['lastname'] = $user['lastname'];
        $_SESSION['logged_in'] = time();

        if($_SESSION['admin'] == false) {
            header('Location: mySchedule.php');
            exit;
        }else{
            header('Location: adminInsert.php');
            exit;
        }

}}

?>
<div class="container" style="width:500px; margin-top:60px">
    <h3 class="text-center">Login</h3><br>
    <form method="post" class="text-center" onsubmit="return validacijaPrijava()">
        <label>Password</label>
        <input type="password" name="password" class="form-control">
        <br>
        <input type="submit" name="login" class="btn btn-info text-center" value="Login">
    </form>
</div>

</body>