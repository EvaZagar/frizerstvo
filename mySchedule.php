<?php
require 'config.php';
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>Schedule</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="btn btn-dark" href="update.php" role="button" style="margin-right: 5px">Services</a>
        </li>
        <li class="nav-item">
            <a href="logout.php" class="btn btn-info">LogOut</a>
        </li>
    </ul>
</nav>
<div class="container" style="margin-top:60px">
    <div class="row text-center" style="margin-bottom: 50px">
        <div class="col-lg-12">
            <h3><?php echo date('d m Y'); ?></h3>
        </div>
    </div>
    <?php
    $sql = "SELECT * FROM reservation WHERE date = CURRENT_DATE";
    foreach ($pdo->query($sql) as $row) {
        $sql = "SELECT username FROM users WHERE id = $row[fk_user]";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $user = $stmt->fetch();
        $dat = date('h:m', strtotime($row['start_time']));
        $id = $row['id'];
        echo "<div class='card text-center' id='$id' style='margin-bottom: 30px'>";
            echo "<div class='card-header'>$dat</div>";
            echo "<div class='card-body'>";
                echo "<ul class='list-group list-group-flush' style='margin-bottom: 10px'>";
                echo "<li class='list-group-item'>$user[username]</li>";
                echo "<li class='list-group-item'>$row[services]</li>";
                echo "</ul>";
            echo "</div>";
            echo "<div class='card-footer text-muted'>";
            echo "<form method='post'>";
                echo "<button id='remove' class='btn btn-success'>Completed</button>";
                echo "</form>";
            echo "</div>";
        echo "</div>";
    }
    ?>
</div>
</body>
</html>