<!DOCTYPE html>
<html lang="en">
<?php
require 'config.php';
session_start();
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/skripta.js"></script>
    <link rel="stylesheet" type="text/css" href="css/stili.css">
    <title>My profile</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="navbar-brand" href="profile.php">My profile</a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="btn btn-dark" href="mySchedule.php" role="button" style="margin-right: 5px">My schedule</a>
        </li>
        <li class="nav-item">
            <form action="logout.php">
                <input type="submit" class="btn btn-info" name="logout" value="Logout"/>
            </form>
        </li>
    </ul>
</nav>
<div class="container" style="padding: 35px">
    <div class="row">
        <div class="col-lg-12">
            <div id="opozorilo"></div>
        </div>
    </div>
</div>
<div class="container text-center" style="width: 500px">
    <div class="row">
        <div class="col-lg-12">
            <form name="userProfile" method="post" onsubmit="return profileValidation()" style="margin-top: 50px">
                <?php
                $sql="SELECT * FROM employees WHERE id ={$_SESSION['user_id']}";
                $stmt = $pdo->prepare($sql);
                $stmt->execute();
                $employee = $stmt->fetch(PDO::FETCH_ASSOC);
                echo "<label for='name'>Name</label>";
                echo "<input type='text' class='form-control' name='username' value='{$employee['name']}'/>";
                echo "<br>";
                echo "<label for='name'>Lastname</label>";
                echo "<input type='text' class='form-control' name='username' value='{$employee['lastname']}'/>";
                echo "<br>";
                echo "<label for='password'>Password</label>";
                echo "<input type='password' class='form-control' name='pass' placeholder='******'/>";
                echo "<br>";
                echo "<label for='password2'>Repeat new password</label>";
                echo "<input type='password' class='form-control' name='pass2' placeholder='******'/>";
                ?>
                <br>
                <input type="submit" class="btn btn-info text-center" name="shrani" value="Save"/>
            </form>
        </div>
    </div>
</div>
<?php


if(isset($_POST['delete']))  {
    $email = $_SESSION['email'];
    $sql = "DELETE FROM users WHERE email=:email";
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue(':email', $email);
    $stmt->execute();
    header("Location: home.php");
}

if(isset($_POST['shrani'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $phone_number = $_POST['phone_number'];
    $id = $_SESSION['user_id'];
    $pass = $_POST['pass'];
    $passwordHash = password_hash($pass, PASSWORD_BCRYPT, array("cost" => 12));
    if($pass == "") {
        $sql = "UPDATE users SET username= :username, email=:email, phone_number=:phone_number WHERE id = :id";

    }else {
        $sql = "UPDATE users SET username= :username, email=:email, phone_number=:phone_number, password=:pass WHERE id = :id";

    }
    $stmt = $pdo->prepare($sql);

    $stmt->bindValue(':username', $username);
    $stmt->bindValue(':email', $email);
    $stmt->bindValue(':phone_number', $phone_number);
    if($pass != null) {
        $stmt->bindValue(':pass', $passwordHash);
    }
    $stmt->bindValue(':id', $id);

    $stmt->execute();
    echo "<meta http-equiv='refresh' content='0'>";
}

?>
</body>

