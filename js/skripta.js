function validacijaRegistracija(){
    var name = document.formReg.username.value;
    var email = document.formReg.email.value;
    var password1 = document.formReg.password.value;
    var password2 = document.formReg.password2.value;

    if (name == "" || email == ""){
        document.getElementById("opozorilo").innerHTML = "<div class = 'alert alert-danger' role = 'alert' id = 'pozor'>";
        document.getElementById("pozor").innerText = "Niste vnesli vseh podatkov!";
        return false;
    }
    if(password1 != password2) {
        document.getElementById("opozorilo").innerHTML = "<div class = 'alert alert-danger' role = 'alert' id = 'pozor'>";
        document.getElementById("pozor").innerText = "Gesli se ne ujemata";
        return false;
    }
    if(password1.length < 6) {
        document.getElementById("opozorilo").innerHTML = "<div class = 'alert alert-danger' role = 'alert' id = 'pozor'>";
        document.getElementById("pozor").innerText = "Geslo mora biti dolgo vsaj 6 znakov!";
        return false;
    }
}

function validacijaPrijava(){
    var email = document.formsn.email.value;
    var password1 = document.formsn.password.value;

    if (email == "" || password1 == null){
        document.getElementById("opozorilo").innerHTML = "<div class = 'alert alert-danger' role = 'alert' id = 'pozor'>";
        document.getElementById("pozor").innerText = "Niste vnesli vseh podatkov!";
        return false;
    }
}

function profileValidation() {
    var name = document.userProfile.username.value;
    var email = document.userProfile.email.value;
    var phone_number = document.userProfile.phone_number.value;
    var pass1 = document.userProfile.pass.value;
    var pass2 = document.userProfile.pass2.value;

    if(name == "" || email == "" || phone_number == "") {
        document.getElementById("opozorilo").innerHTML = "<div class = 'alert alert-danger' role = 'alert' id = 'pozor'>";
        document.getElementById("pozor").innerText = "Niste vnesli vseh podatkov!";
        return false;
    }
    if(pass1 != pass2) {
        document.getElementById("opozorilo").innerHTML = "<div class = 'alert alert-danger' role = 'alert' id = 'pozor'>";
        document.getElementById("pozor").innerText = "Gesli se ne ujemata!";
        return false;
    }
    if(pass1.length < 6) {
        document.getElementById("opozorilo").innerHTML = "<div class = 'alert alert-danger' role = 'alert' id = 'pozor'>";
        document.getElementById("pozor").innerText = "Geslo mora biti dolgo vsaj 6 znakov";
        return false;
    }

}
