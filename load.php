<?php

require 'config.php';

$data = array();
$query = "SELECT * FROM reservation ORDER BY id";
$statement = $pdo->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
foreach($result as $row){
    $sql = "SELECT username FROM users WHERE id = {$row['fk_user']}";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $res = $stmt->fetch();
    $data[] = array(
        'id'   => $row["id"],
        'services' => $row['services'],
        'start'   => $row["start_time"],
        'end'   => $row["end_time"]
    );
}
echo json_encode($data);

?>
