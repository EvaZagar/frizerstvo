<?php
session_start();
require 'config.php';

if(isset($_POST["title"])) {
    $string_var = '';
    foreach ($_SESSION['array_izbire'] as $value) {
        $sql = "SELECT name FROM service WHERE id = {$value}";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $service = $stmt->fetch();
        $string_var = $string_var." ".$service[name].",";
    }
    $query = "INSERT INTO reservation (start_time, end_time, date, services, fk_user) VALUES (:start_event, :end_event, :date, :services, :fk_user)";
    $statement = $pdo->prepare($query);
    $statement->execute(
        array(
            ':start_event' => $_POST['start'],
            ':end_event' => $_POST['end'],
            ':date' => $_POST['date'],
            ':services' => $string_var,
            ':fk_user'  => $_SESSION['user_id']
        )
    );
}
?>

