<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Da vsilimo mobilni pogled na mobilnih napravah in da bo zoom s prsti pravilno podprt -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/skripta.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/stili.css">
    <title>Service update</title>
</head>
<?php
session_start();
require 'config.php';
?>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="navbar-brand" href="update.php">Services</a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="btn btn-dark" href="mySchedule.php" role="button" style="margin-right: 5px">My schedule</a>
        </li>
        <li class="nav-item">
            <form action="logout.php">
                <input type="submit" class="btn btn-info" name="logout" value="Logout"/>
            </form>
        </li>
    </ul>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert" id="message" style="visibility: hidden; margin-top: 30px"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <h4>Categories</h4>
            <form name="categoryEdit" method="post">
                <select name="category" class="browser-default custom-select" style="margin-bottom: 10px">
                <?php
                $sql = "SELECT id, name FROM servicecategory";
                foreach ($pdo->query($sql) as $row) {
                    echo "<option value='$row[id]'>$row[name]</option>";
                }
                ?>
                </select>
                <br>
                <input type="submit" class="btn btn-info" name="editCat" value="Edit category"/>
                <input type="submit" class="btn btn-info" name="delCat" value="Delete category"/>
                <input type="submit" class="btn btn-dark" name="catAdd" value="Add category"/>
                <br>
                <?php
                    if(isset($_POST['editCat'])) {
                        $category = $_POST['category'];
                        $sql = "SELECT * FROM servicecategory WHERE id = {$category}";
                        $stmt = $pdo->prepare($sql);
                        $stmt->execute();
                        $cat = $stmt->fetch(PDO::FETCH_ASSOC);
                        echo "<br>";
                        echo "<input type='text' class='form-control' value='{$cat['name']}' name='catName'/>";
                        echo "<br>";
                        echo "<input type='submit' class='btn btn-info' value='Save changes' name='catSave'/>";
                        echo "<input type='text' class='form-control' style='visibility: hidden' value='{$cat['id']}' name='catId'/>";
                    }
                    ?>
            </form>
            <form method="post" id="addCategory" style="visibility: hidden">
                <label>Category name</label>
                <input type="text" class="form-control" name="kat"/>
                <br>
                <input type="submit" class="btn btn-dark form-control" name="addCat" value="Add"/>
                <br>
            </form>
            <?php
                    if(isset($_POST['catSave'])) {
                        $catId = $_POST['catId'];
                        $catName = $_POST['catName'];
                        $sql = "UPDATE servicecategory SET name = :name WHERE id = {$catId}";
                        $stmt = $pdo->prepare($sql);
                        $stmt->bindValue(':name', $catName);
                        $stmt->execute();
                        echo "<meta http-equiv='refresh' content='0'>";

                    }
                    if(isset($_POST['delCat'])) {
                        $category = $_POST['category'];
                        $sql = "DELETE FROM servicecategory WHERE id = {$category}";
                        $stmt3 = $pdo->prepare($sql);
                        $stmt3->execute();
                        echo "<meta http-equiv='refresh' content='0'>";

                    }
                    if(isset($_POST['catAdd'])) {
                        echo"<script>$('#addCategory').css('visibility', 'visible');</script>";
                    }
                    if(isset($_POST['addCat'])){
                        $name = !empty($_POST['kat']) ? trim($_POST['kat']) : null;
                        $sql = "SELECT COUNT(name) AS num FROM servicecategory WHERE name = :kat";
                        $stmt = $pdo->prepare($sql);
                        $stmt->bindValue(':kat', $name);
                        $stmt->execute();
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if($row['num'] > 0){
                            echo "<script>$('.alert').addClass('alert-danger');
                                         $('#message').css('visibility', 'visible');
                                         $('#message').text('Category already exists');
                                </script>";
                        }else {
                            $sql = "INSERT INTO servicecategory (name) VALUES (:kat)";
                            $stmt = $pdo->prepare($sql);
                            $stmt->bindValue(':kat', $name);
                            $result = $stmt->execute();
                            echo "<meta http-equiv='refresh' content='0'>";
                        }
                    }
                    ?>
        </div>
        <div class="col-lg-6">
            <h4>Services</h4>
            <form name="serviceEdit" method="post">
                <select name="service" class="browser-default custom-select" style="margin-bottom: 10px">
                <?php
                $sql = "SELECT * FROM service";
                    foreach ($pdo->query($sql) as $row) {
                        echo "<option value='$row[id]'>$row[name]</option>";
                    }
                ?>
                </select>
                <br>
                <input type="submit" name="editService" class="btn btn-info" value="Edit service"/>
                <input type="submit" name="delService" class="btn btn-info" value="Delete service"/>
                <input type="submit" name="addService1" class="btn btn-dark" value="Add service"/>
            </form>
            <form method='post'>
                <br>
            <select name="izbira" id="izbira" class="browser-default custom-select" style="visibility: hidden;">
                <?php
                $sql="SELECT id, name FROM servicecategory";
                foreach ($pdo->query($sql) as $row){
                    echo "<option value='$row[id]'>$row[name]</option>";
                }
                ?>
            </select>
            <?php
                if(isset($_POST['editService'])) {
                    echo"<script>$('#izbira').css('visibility', 'visible');</script>";
                    $ser = $_POST['service'];
                    $sql = "SELECT * FROM service WHERE id = {$ser}";
                    $stmt = $pdo->prepare($sql);
                    $stmt->execute();
                    $service = $stmt->fetch(PDO::FETCH_ASSOC);
                    echo "<br>";
                    echo "<label>Service</label>";
                    echo "<input type='text' class='form-control' name='name' value='{$service['name']}'/>";
                    echo "<br>";
                    echo "<label>Price</label>";
                    echo "<input type='number' class='form-control' name='price' value='{$service['price']}'/>";
                    echo "<br>";
                    echo "<label>Duration(in minutes)</label>";
                    echo "<input type='number' class='form-control' name='duration' value='{$service['duration']}'/>";
                    echo "<input name='serviceId' value='{$service['id']}' style='visibility: hidden'/>";
                    echo "<br>";
                    echo "<input type='submit' class='btn btn-info' name='serviceSave' value='Save changes'/>";
                }
                ?>
                </form>
                <form id="addStoritev" method="post" style="visibility: hidden">
                    <label>Izberite kategorijo storitve</label>
                    <br>
                    <select name="izbira" class="browser-default custom-select">
                        <?php
                        require 'config.php';
                        $sql="SELECT id, name FROM servicecategory";

                        foreach ($pdo->query($sql) as $row){

                            echo "<option value='$row[id]'>$row[name]</option>";

                        }
                        ?>
                    </select>
                    <br>
                    <label>Vnesite storitev</label>
                    <input type="text" class="form-control" name="storitev"/>
                    <br>
                    <label>Vnesite ceno storitve</label>
                    <input type="number" class="form-control" name="price"/>
                    <br>
                    <label>Vnesite trajanje storitve (v min)</label>
                    <input type="number" class="form-control" name="time"/>
                    <br>
                    <input type="submit" class="btn btn-dark form-control" name="addService" value="Add"/>
                </form>
            <?php
                if(isset($_POST['serviceSave'])) {
                    $name = $_POST['name'];
                    $price = $_POST['price'];
                    $duration = $_POST['duration'];
                    $cat = $_POST['izbira'];
                    $id = $_POST['serviceId'];

                    $sql = "UPDATE service SET name = :name, price = :price, duration = :duration, fk_servicecategory = :cat WHERE id = {$id}";
                    $stmt = $pdo->prepare($sql);
                    $stmt->bindValue(":name", $name);
                    $stmt->bindValue(":price", $price);
                    $stmt->bindValue(":duration", $duration);
                    $stmt->bindValue(":cat", $cat);
                    $stmt->execute();
                    echo "<meta http-equiv='refresh' content='0'>";
                }
                if(isset($_POST['delService'])) {
                    $id = $_POST['service'];
                    $sql = "DELETE FROM service WHERE id = {$id}";
                    $stmt = $pdo->prepare($sql);
                    $stmt->execute();
                    echo "<meta http-equiv='refresh' content='0'>";
                }
                if(isset($_POST['addService1'])) {
                    echo"<script>$('#addStoritev').css('visibility', 'visible');</script>";
                }
            if(isset($_POST['addService'])){

                $name = !empty($_POST['storitev']) ? trim($_POST['storitev']) : null;
                $price = !empty($_POST['price']) ? trim($_POST['price']) : null;
                $duration = !empty($_POST['time']) ? trim($_POST['time']) : null;

//Construct the SQL statement and prepare it.
                $sql = "SELECT COUNT(name) AS num FROM service WHERE name = :storitev";
                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':storitev', $name);

                $stmt->execute();

                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                if($row['num'] > 0){
                    echo "<script>$('.alert').addClass('alert-danger');
                                         $('#message').css('visibility', 'visible');
                                         $('#message').text('Service already exists');
                                </script>";
                }else {
                    $fk = $_POST['izbira'];
                    $sql = "INSERT INTO service (name, price, duration, fk_servicecategory) VALUES (:storitev, :price, :duration, :fk)";
                    $stmt = $pdo->prepare($sql);
                    $stmt->bindValue(':storitev', $name);
                    $stmt->bindValue(':price', $price);
                    $stmt->bindValue(':duration', $duration);
                    $stmt->bindValue(':fk', $fk);
                    $result = $stmt->execute();
                    echo "<meta http-equiv='refresh' content='0'>";
                }

            }
            ?>
        </div>
    </div>
</div>
</body>


